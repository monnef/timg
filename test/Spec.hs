{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

import           Test.Framework

import {-@ HTF_TESTS @-} Tests

main :: IO ()
main = htfMain htf_importedTests
