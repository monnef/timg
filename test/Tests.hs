{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE OverloadedStrings #-}

module Tests where

import           Test.Framework

import Lib

test_linesOf:: IO ()
test_linesOf = do
  assertEqual [] $ linesOf 0 0 ' '
  assertEqual ["###", "###"] $ linesOf 3 2 '#'
