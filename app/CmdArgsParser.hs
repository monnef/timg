module CmdArgsParser where

import BuildInfo
import qualified Data.Text as T
import Lib
import Options.Applicative
import qualified Options.Applicative as OA
import qualified Options.Applicative.Help as D
import Text.Read (readMaybe)
import Utils

surround1 :: String -> String -> String
surround1 s x = s <> x <> s

surround2 :: String -> String -> String -> String
surround2 l r x = l <> x <> r

readChar :: String -> Maybe Char
readChar = surround1 "'" >>> readMaybe

readVec2I :: String -> Maybe Vec2I
readVec2I = surround2 "(" ")" >>> readMaybe

readImgCmdType :: String -> Maybe ImgCmdType
readImgCmdType = readMaybe

charReader :: ReadM Char
charReader = maybeReader readChar

vec2Reader :: ReadM Vec2I
vec2Reader = maybeReader readVec2I

textReader :: ReadM Text
textReader = maybeReader $ T.pack >>> Just

imgCmdTypeReader :: ReadM ImgCmdType
imgCmdTypeReader = maybeReader readImgCmdType

cmdArgsParser :: Parser ImageCmdArgs
cmdArgsParser =
  ImageCmdArgs
    <$> OA.argument textReader (metavar "PATH" <> help "File path or URL") --
    <*> option vec2Reader (long "size" <> short 's' <> metavar "WIDTH,HEIGHT" <> sizeHelp)
    <*> optional (option vec2Reader (long "position" <> short 'p' <> metavar "X,Y" <> posHelp))
    <*> switch (short '0' <> long "zero-based" <> zeroBasedHelp)
    <*> option imgCmdTypeReader (long "mode" <> short 'm' <> value Filled <> metavar "MODE" <> modeHelp)
    <*> option charReader (long "replace-character" <> long "rc" <> value '#' <> metavar "CHAR" <> replCharHelp)
    <*> switch (long "debug" <> short 'd')
    <*> switch (long "debug-pos" <> long "dp" <> debugPosHelp)
  where
    sizeHelp = help "Size in characters"
    posHelp = help "Position on in screen in characters"
    zeroBasedHelp = help "Switches position from 1-based (default) to 0-based"
    modeHelp = help "Image drawing mode, valid values: Stretched, Centered, Filled (default)"
    replCharHelp =
      help "Replace character (default is '#', used by Terminology to determine area where to put an image)"
    debugPosHelp = help "Don't draw an image, just print a rectangle filled with replace characters"

cmdArgsParserInfo :: ParserInfo ImageCmdArgs
cmdArgsParserInfo =
  OA.info
    (cmdArgsParser <**> helper)
    (fullDesc <> progDescDoc (Just doc))
  where
    doc =
      D.text "Shows an image in the Terminology terminal emulator."
        <> D.line
        <> D.text "✨ Made by monnef in Haskell."
        <> D.line
        <> D.text "GPL 3"
        <> D.line
        <> D.text ("Version: " <> toS versionText)
        <> D.line
        <> D.text ("Build info: " <> toS extendedBuildInfo)

parseCmdArgs :: IO ImageCmdArgs
parseCmdArgs = execParser cmdArgsParserInfo
