{-# LANGUAGE OverloadedStrings #-}

module Main where

import CmdArgsParser
import Control.Lens
import Control.Monad (when)
import Lib
import Utils

main :: IO ()
main = do
  args <- parseCmdArgs
  when (args ^. debug) $ print args
  args & putImage & void
