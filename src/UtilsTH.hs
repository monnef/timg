{-# LANGUAGE OverloadedStrings #-}

module UtilsTH where

import Control.Arrow ((>>>))
import Control.Lens ((&))
import Data.Char (toLower, toUpper)
import Data.Maybe (fromJust)
import qualified Data.Text as T

convertFieldName :: String -> String -> String
convertFieldName prefix x = x & drop (length prefix) & deCapitalizeWordStr

deCapitalizeWordStr :: String -> String
deCapitalizeWordStr "" = ""
deCapitalizeWordStr x = (x & head & toLower) : (x & tail)

capitalizeWordStr :: String -> String
capitalizeWordStr "" = ""
capitalizeWordStr x = (x & head & toUpper) : (x & tail)
