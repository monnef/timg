{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

module Utils
  ( tshow,
    tPutStr,
    tPutStrLn,
    tPutStrLnLn,
    tErrPutStr,
    tErrPutStrLn,
    tReadFile,
    tReadFileEither,
    tWriteFile,
    flushStdOut,
    flushStdErr,
    flushStds,
    (>>>),
    (&),
    (<&>),
    ($>),
    (<$),
    Text,
    bool,
    orCrash,
    skip,
    toS,
    forM,
    forM_,
    void,
    qq,
    nl,
    pShow,
    firstErrorFromEithers,
    stripSuffixIfPresent,
    capitalize,
    deCapitalize,
    liftToText,
    indent,
    backSlashToSlash,
    hashMapLookupValue,
    leftOnTrue,
    emptyTextAsNothing,
    lift,
    fmtFloat,
    textToMaybe,
    justFalseToNothing,
    lookupPair,
    unzipMaybe,
    eq,
    surround,
    filterOut
  )
where

-- import qualified Data.ByteString.Lazy as BL
-- import qualified Data.ByteString.Lazy.Char8 as CL

-- import qualified Data.Text.Lazy as TL
-- import qualified Data.Text.Lazy.Encoding as TLE

import Control.Arrow ((>>>))
import Control.Exception (SomeException, try)
import Control.Monad (forM, forM_, void)
import Control.Monad.Identity (Identity)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Writer.Lazy (Writer)
import Data.Bool (bool)
import Data.Either.Extra (mapLeft)
import Data.Function ((&))
import Data.Functor (($>), (<&>))
import Data.Functor.Identity (runIdentity)
import qualified Data.HashMap.Lazy as M
import Data.Hashable (Hashable)
import Data.List (find)
import Data.Maybe (fromMaybe)
import Data.String.Conv (toS)
import Data.Text (Text)
import qualified Data.Text as T
import Numeric (showFFloat)
import System.IO (hFlush, hPutStr, hPutStrLn, stderr, stdout)
import System.IO.Extra (IOMode (..), hSetEncoding, utf8_bom, withFile)
import System.IO.Strict (hGetContents)
import Text.InterpolatedString.Perl6 (qq)
import qualified Text.Pretty.Simple as PrettySimple
import UtilsTH (capitalizeWordStr, deCapitalizeWordStr)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tPutStr :: Text -> IO ()
tPutStr = T.unpack >>> putStr

tPutStrLn :: Text -> IO ()
tPutStrLn = T.unpack >>> putStrLn

tPutStrLnLn :: Text -> IO ()
tPutStrLnLn x = x <> "\n" & tPutStrLn

tErrPutStr :: Text -> IO ()
tErrPutStr = toS >>> hPutStr stderr

tErrPutStrLn :: Text -> IO ()
tErrPutStrLn = toS >>> hPutStrLn stderr

flushStdOut :: IO ()
flushStdOut = hFlush stdout

flushStdErr :: IO ()
flushStdErr = hFlush stderr

flushStds :: IO ()
flushStds = flushStdOut >> flushStdErr

-- | Strict file read in UTF8 encoding.
tReadFile :: Text -> IO Text
tReadFile fileName = do
  withFile (toS fileName) ReadMode $
    \h -> do
      hSetEncoding h utf8_bom
      contents <- System.IO.Strict.hGetContents h
      return $ toS contents

tReadFileEither :: Text -> IO (Either Text Text)
tReadFileEither fileName = do
  c :: Either SomeException Text <- try $ tReadFile fileName
  return $ mapLeft tshow c

tWriteFile :: Text -> Text -> IO ()
tWriteFile fileName contents = writeFile (toS fileName) (toS contents)

orCrash :: Show e => Either e a -> a
orCrash (Right x) = x
orCrash (Left e) = error $ show e

skip :: Monad m => m ()
skip = return ()

nl :: Text
nl = "\n"

pShow :: Show a => a -> Text
pShow = PrettySimple.pShowOpt opts >>> toS
  where
    opts = PrettySimple.defaultOutputOptionsDarkBg {PrettySimple.outputOptionsIndentAmount = 2}

firstErrorFromEithers :: [Either l r] -> Either l [r]
firstErrorFromEithers [] = Right []
firstErrorFromEithers ((Left e) : _) = Left e
firstErrorFromEithers ((Right r) : xs) = firstErrorFromEithers xs <&> (r :)

stripSuffixIfPresent :: Text -> Text -> Text
stripSuffixIfPresent s x = x & T.stripSuffix s & fromMaybe x

capitalize :: Text -> Text
capitalize = toS >>> capitalizeWordStr >>> toS

deCapitalize :: Text -> Text
deCapitalize = toS >>> deCapitalizeWordStr >>> toS

liftToText :: (String -> String) -> (Text -> Text)
liftToText f = toS >>> f >>> toS

indent :: Int -> Text -> Text
indent n x = x & T.lines <&> (T.replicate n " " <>) & T.unlines

backSlashToSlash :: Text -> Text
backSlashToSlash = T.replace "\\" "/"

hashMapLookupValue :: Eq v => v -> M.HashMap k v -> Maybe k
hashMapLookupValue x m = m & M.toList & find (\(_, v) -> v == x) <&> fst

leftOnTrue :: l -> Bool -> Either l ()
leftOnTrue _ False = skip
leftOnTrue l True = Left l

emptyTextAsNothing :: Text -> Maybe Text
emptyTextAsNothing "" = Nothing
emptyTextAsNothing x = Just x

fmtFloat :: RealFloat a => a -> Text
fmtFloat x = showFFloat Nothing x "" & toS

textToMaybe :: Text -> Maybe Text
textToMaybe "" = Nothing
textToMaybe x = Just x

justFalseToNothing :: Maybe Bool -> Maybe Bool
justFalseToNothing (Just False) = Nothing
justFalseToNothing x = x

lookupPair :: (Eq k, Hashable k) => k -> M.HashMap k v -> Maybe (k, v)
lookupPair k x = M.lookup k x <&> (k,)

unzipMaybe :: Maybe (a, b) -> (Maybe a, Maybe b)
unzipMaybe Nothing = (Nothing, Nothing)
unzipMaybe (Just (x, y)) = (Just x, Just y)

eq :: Eq a => a -> a -> Bool
eq = (==)

surround :: Text -> Text -> Text -> Text
surround l r x = l <> x <> r

filterOut :: (a -> Bool) -> [a] -> [a]
filterOut f = filter (f >>> not)
