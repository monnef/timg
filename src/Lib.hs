{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}

module Lib
  ( putImage
  , ImageCmdArgs(..)
  , debug
  , ImgCmdType(..)
  , linesOf
  , Vec2I
  ) where

import           Control.Arrow        ((>>>))
import           Control.Lens
import           Control.Monad        (void)
import           Control.Monad.Except (ExceptT, runExceptT)
import           Control.Monad.Trans  (liftIO)
import           Data.Bool            (bool)
import           Data.Functor         (($>))
import           Data.Maybe           (isJust, isNothing, maybeToList)
import           Data.Text            (Text)
import qualified Data.Text            as T
import qualified Data.Text.IO         as TIO
import           Debug.Trace          (trace)
import           GHC.Generics         (Generic)

type Vec2I = (Int, Int)

data ImgCmdType
  = Stretched
  | Centered
  | Filled
  deriving (Show, Eq, Generic, Read)

data ImageCmdArgs = ImageCmdArgs
  { _path             :: Text
  , _size             :: Vec2I
  , _pos              :: Maybe Vec2I
  , _zeroBasedPos     :: Bool
  , _cmdType          :: ImgCmdType
  , _replaceCharacter :: Char
  , _debug            :: Bool
  , _debugPos         :: Bool
  } deriving (Show, Eq, Generic)

makeFieldsNoPrefix ''ImageCmdArgs

simpleImageCmdArgs :: Int -> Int -> Text -> ImageCmdArgs
simpleImageCmdArgs w h p =
  ImageCmdArgs
    { _replaceCharacter = '#'
    , _size = (w, h)
    , _path = p
    , _cmdType = Filled
    , _debug = False
    , _debugPos = False
    , _pos = Nothing
    , _zeroBasedPos = False
    }

data RawCommand = RawCommand
  { _prefix :: Text
  , _body   :: Text
  , _suffix :: Text
  } deriving (Show, Eq, Generic)

makeFieldsNoPrefix ''RawCommand

tyCmdFromText :: Text -> RawCommand
tyCmdFromText body = RawCommand {_prefix = "\ESC}", _body = body, _suffix = "\NUL"}

tyBeginMediaReplaceCmd :: RawCommand
tyBeginMediaReplaceCmd = tyCmdFromText "ib"

tyEndMediaReplaceCmd :: Bool -> RawCommand
tyEndMediaReplaceCmd insNewLn = tyCmdFromText "ie" & process
  where
    process = bool id (suffix %~ (<> "\n")) insNewLn

printTextCmd :: Text -> RawCommand
printTextCmd x = RawCommand {_prefix = "", _body = x, _suffix = ""}

moveCursorCmd :: (Int, Int) -> RawCommand
moveCursorCmd (x, y) = RawCommand {_prefix = "\ESC[", _body = tshow y <> ";" <> tshow x, _suffix = "f"}

saveCursorPosCmd :: RawCommand
saveCursorPosCmd = RawCommand {_prefix = "\ESC[", _body = "", _suffix = "s"}

restoreCursorPosCmd :: RawCommand
restoreCursorPosCmd = RawCommand {_prefix = "\ESC[", _body = "", _suffix = "u"}

newtype Error =
  Error Text

type Stack a = ExceptT Error IO a

tshow :: Show a => a -> Text
tshow = show >>> T.pack

infixl 1 &>

(&>) :: Functor f => f b -> a -> f a
(&>) = ($>)

linesOf :: Int -> Int -> Char -> [Text]
linesOf w h c = T.replicate w (T.pack [c]) & replicate h

imageCommands :: ImageCmdArgs -> [RawCommand]
imageCommands args = before ++ start ++ fill ++ after
  where
    w = args ^. size . _1
    h = args ^. size . _2
    rc = args ^. replaceCharacter
    p = args ^. path
    t = args ^. cmdType
    ps = args ^. pos
    dbgPos = args ^. debugPos
    emptyPos = ps & isNothing
    lns = linesOf w h rc
    fill = lns & itoList <&> cmd & concat
    cmd :: (Int, Text) -> [RawCommand]
    cmd (idx, x) =
      [ cmdStart idx
      , bool [tyBeginMediaReplaceCmd] [] dbgPos
      , [printTextCmd x]
      , bool [tyEndMediaReplaceCmd emptyPos] (bool [] [printTextCmd "\n"] emptyPos) dbgPos
      ] &
      concat
    cmdStart idx = ps <&> _2 %~ (+ idx) <&> handlePosBase <&> moveCursorCmd & maybeToList
    start = [tyCmdFromText $ typeStr t <> T.singleton rc <> tshow w <> ";" <> tshow h <> ";" <> p]
    typeStr Stretched = "is"
    typeStr Centered  = "ic"
    typeStr Filled    = "if"
    before = bool [saveCursorPosCmd] [] emptyPos
    after = bool [restoreCursorPosCmd] [] emptyPos
    handlePosBase = bool id (both +~ 1) $ args ^. zeroBasedPos

printCommand :: RawCommand -> Stack ()
printCommand x = x ^. prefix <> x ^. body <> x ^. suffix & TIO.putStr & liftIO

type StackOut a = IO (Either Error a)

stackOut :: Stack a -> StackOut a
stackOut = runExceptT

putImage :: ImageCmdArgs -> StackOut ()
putImage args = args & imageCommands & dbg & mapM printCommand &> () & stackOut
  where
    dbg = bool id (\x -> trace (x & map (show >>> (<> "\n")) & concat) x) $ args ^. debug
