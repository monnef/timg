{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module BuildInfo where

import qualified Data.Text as T
import Data.Version (showVersion)
import GitHash
import Paths_timg
import Utils

versionText :: Text
versionText = showVersion version & toS

gi :: GitInfo
gi = $$tGitInfoCwd

extendedBuildInfo :: Text
extendedBuildInfo = T.strip [qq|{gitBranchAndLongHash} ({giCommitDate gi}) {gitTagText} {gitDirtyText}|]

gitBranchAndLongHash :: Text
gitBranchAndLongHash = toS $ giBranch gi <> "@" <> giHash gi

gitShortHash :: Text
gitShortHash = toS $ take 8 (giHash gi)

gitBranchAndShortHash :: Text
gitBranchAndShortHash = toS $ giBranch gi <> "@" <> toS gitShortHash

isGitDirty :: Bool
isGitDirty = giDirty gi

gitDirtyText :: Text
gitDirtyText = bool "" "[DIRTY]" isGitDirty

gitTagText :: Text
gitTagText = [qq|<{giTag gi}>|]
