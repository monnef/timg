# timg

Shows image in [Terminology](https://www.enlightenment.org/about-terminology) (terminal emulator). Supports position, size, all image rendering modes and custom replace character.

![](screencast.gif)

# Installation

## Requirements

* [stack](http://haskellstack.org)

## Install to a stack bin directory

```sh
stack install
```

## I want just a binary

```sh
chmod u+x build_dist.sh
./build_dist.sh
```

A fresh binary will be in the `dist` directory.

# License
**GPL3**
